$(function(){
    // updateRow()
    deleteRow()
    directView()
})

function updateRow(subject, content) {
    $('.update-btn, .view-experince').on("click",function(){
        $.ajax({
            url: "/",
            type: "POST",
            data: { 'subject': subject, 'content': content },
            success: function (data) {
    
            }
        });
    })
}

function deleteRow() {
    $('.delete-experience, .delete-btn').on("click",function(e){
        const $target = $(e.target);
        const id = $target.attr('data-id');
        $.ajax({
            url: "/experience/" + id,
            type: "DELETE",
            data: { 'id': id },
            success: function (data) {
                location.replace("http://localhost:3000/experience/");
            },
        });
    })

}

function directView(){
    $('.view-experince').on("click",function(){
        console.log("click view")
        location.replac("http://localhost:3000/experience/");
    })
}