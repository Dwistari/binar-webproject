const express = require('express')
const app = express()
const PORT = process.env.PORT || 3000
const session = require('express-session')
const flash = require('express-flash')
const path = require('path');

app.use(express.urlencoded({extended : false}))
app.use(express.json())

app.use(session({
    secret : 'Buat ini jadi rahasia',
    resave : false,
    saveUninitialized : false
}))

const passport = require ('./lib/passport')
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

app.set ('view engine', 'ejs');

app.get('/', (req,res) => {
    res.sendFile(path.join(__dirname+'/views/index.html'));
});

app.use(express.static(path.join(__dirname,"/")));

const router = require('./router')
app.use (router)
app.listen(PORT,() => {
    console.log(`Example app listening http://localhost:${PORT}`)
})


