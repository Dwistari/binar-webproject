const router = require ('express').Router();

const homeRouter = require ('./homeRouter');
const adminRouter = require ('./adminRouter');
const experienceRouter = require ('./experienceRouter');
const skillsRouter = require ('./skillsRouter');


router.use('/', homeRouter);
router.use('/admin', adminRouter);
router.use('/experience', experienceRouter);
router.use('/skills', skillsRouter);


module.exports = router;

