const router = require ('express').Router();
const adminController = require ('../controller/adminController');
const restrict = require('../middlewares/restrict')


//Home Admin
router.get('/',(req,res) => res.render('index'));

//Login
router.get('/login',(req,res) => res.render('login'));
router.post('/login', adminController.login);

router.get('/', restrict, (req, res) => res.render('index'))
router.get('/whoami', restrict, adminController.whoami)

//Register
router.get('/register',(req,res) => res.render('register'));
router.post('/register', adminController.register);

module.exports = router
