const router = require ('express').Router();

const homeController = require ('../controller/homeController');

router.get('/inbox',homeController.index);
router.post('/send',homeController.create);

module.exports = router
