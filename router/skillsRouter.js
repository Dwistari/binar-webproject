const router = require ('express').Router();

const skillsController = require ('../controller/skillsController');

router.get('/',skillsController.index);
router.post('/add',skillsController.create);
router.delete('/:id',skillsController.delete);

module.exports = router
