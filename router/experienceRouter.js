const router = require ('express').Router();

const experienceController = require ('../controller/experienceController');

//Experience list
router.get('/', experienceController.index);

router.get("/:id", experienceController.show);
router.post('/add',experienceController.create);
router.delete('/:id',experienceController.delete);

module.exports = router
