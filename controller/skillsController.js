const { Skill } = require('../models')

module.exports = {
    index: (req, res) => {
        Skill.findAll({})
            .then(response => {
                if (response !== 0) {
                    res.json({
                        'status': 200,
                        'message': 'berhasil',
                        'data': response
                    })
                } else {
                    res.json({
                        'status': 400,
                        'message': 'data tidak ditemukan',
                        'data': response
                    })
                }

            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'kesalahan server'
                })
            })
    },

    create: (req, res) => {
        const { name, progress } = req.body;
        Skill.create({
            name,
            progress

        })
            .then(job => {
                res.redirect('/admin')
            })
            .catch(err => {
                res.send(`Gagal menambahkan pengalaman, karena 
            ${JSON.stringify(err.message, null, 2)}`)
            })
    },

    delete: (req, res) => {
        const skillId = req.params.id
        Skill.destroy({
            where: {
                id: skillId
            }
        })
            .then(response => {
                res.json({
                    'status': 200,
                    'message': 'Data berhasil dihapus',
                    'data': response
                })
            })
    }

}