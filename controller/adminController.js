const { Admin } = require('../models')
const passport = require('../lib/passport')


function format(user) {
    const { id, username } = user
    return {
        id,
        username,
        accessToken : user.generateToken()
    }
}

module.exports = {
    register: (req, res, next) => {
        Admin.register(req.body)
            .then(() => {
                res.redirect('/admin/login')
            })
            .catch(err => next(err))
    },

    login: (req, res) => {
        Admin.authenticate (req.body)
        .then(user => {
        res.json(
        format(user)
        )
        })
    },
    
    // loginAdmin: passport.authenticate('local', {
    //     successRedirect: '/',
    //     failureRedirect: '/admin/login',
    //     failureFlash: true // Untuk mengaktifkan express flash
    // }),

    whoami: (req, res) => {
        // res.render('profile', req.user.dataValues)
        const currentUser = req.user;
        res.json(currentUser)
    }
}
