const { Inbox } = require('../models')

module.exports = {

    index: (req, res) => {
        Inbox.findAll({})
            .then(response => {
                if (response !== 0) {
                    res.json({
                        'status': 200,
                        'message': 'berhasil',
                        'data': response
                    })
                } else {
                    res.json({
                        'status': 400,
                        'message': 'data tidak ditemukan',
                        'data' : response
                    })
                }

            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'kesalahan server'
                })
            })
    },

    create: (req, res) => {
        const { name, email, subject,message } = req.body;
        Inbox.create({
            name,
            email,
            subject,
            message
        })
            .then(inbox => {
                console.log(inbox)
                res.redirect('/#contact')
            })
        .catch (err => {
            res.send (`Gagal mengirim pesan, karena 
            ${JSON.stringify(err.message,null,2)}`)
        })
    }
}