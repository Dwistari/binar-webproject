const { Experience } = require('../models')

module.exports = {
    index: (req, res) => {
        Experience.findAll({})
            .then(response => {
            // res.render("experience", { title: "experience", experiences : response});

                if (response !== 0) {
                    res.json({
                        'status': 200,
                        'message': 'berhasil',
                        'data': response
                    })
                } else {
                    res.json({
                        'status': 400,
                        'message': 'data tidak ditemukan',
                        'data' : response
                    })
                }
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'kesalahan server'
                })
            })
    },

    show: (req,res) => {
        const experienceId = req.params.id
        Experience.findOne({
            where: {id : experienceId} 
        })
        .then(data => {
            res.render("experience", { title: "experience", experiences : data});

        })
        .catch(err => res.status(404).send('Tidak menemukan article'))
      },

    create: (req, res) => {
        const { title, joindate, company,description } = req.body;
        Experience.create({
            title,
            joindate  ,
            company ,
            description
      
        })
            .then(job => {
                res.redirect('/admin')
            })
        .catch (err => {
            res.send (`Gagal menambahkan pengalaman, karena 
            ${JSON.stringify(err.message,null,2)}`)
        })
    },
    
    delete: (req,res) => {
        const experienceId = req.params.id
        Experience.destroy({
            where :{
                id : experienceId
            }
        })
        .then(response => {
            res.redirect('/');
        })
    }
}
